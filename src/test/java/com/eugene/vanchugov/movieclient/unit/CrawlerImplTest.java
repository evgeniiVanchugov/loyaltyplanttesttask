package com.eugene.vanchugov.movieclient.unit;

import com.eugene.vanchugov.movieclient.crawler.RestTemplateRequestProvider;
import com.eugene.vanchugov.movieclient.crawler.impl.CrawlerImpl;
import com.eugene.vanchugov.movieclient.crawler.impl.CrawlerProcessSingleton;
import com.eugene.vanchugov.movieclient.model.Movie;
import com.eugene.vanchugov.movieclient.model.ResponseModel;
import com.eugene.vanchugov.movieclient.util.ThreadUtils;
import com.eugene.vanchugov.movieclient.util.TimeoutHelper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class CrawlerImplTest {

    private static final int TOTAL_RESULTS = 19032;
    private static final int RESULTS_ON_PAGE = 10;
    private static final int TOTAL_PAGES = 1900;
    private static final int TIMEOUT_SECONDS = 5;

    private CrawlerImpl crawler;
    private RestTemplateRequestProvider requestProvider;
    private int latency;
    private CrawlerProcessSingleton process;

    private Random random = new Random();

    @Before
    public void setUp() {
        requestProvider = Mockito.mock(RestTemplateRequestProvider.class);
        process = CrawlerProcessSingleton.getInstance();
        crawler = new CrawlerImpl();
        crawler.setRequestProvider(requestProvider);
    }

    @Test(expected = IllegalArgumentException.class)
    public void submitJob_offsetLessThanZero() {
        crawler.submitJob(-1, 10, false);
    }

    @Test
    public void submitJob() {
        latency = random.nextInt(100);
        getWithLatency(latency);

        crawler.submitJob(1, 1, false);

        System.out.println(process.getProgressMessage());
    }

    @Test
    public void submitJob_timeout() {
        latency = 10000;
        getWithLatency(latency);

        crawler.submitJob(2, 1, false);

        System.out.println(process.getProgressMessage());
    }

    private void getWithLatency(int latency) {
        when(requestProvider.getPage(anyInt())).then(invocationOnMock -> {

            List<Movie> moviesWithRandomIds = Stream.generate(() ->
                    Movie.builder()
                            .id(random.nextInt(TOTAL_RESULTS))
                            .build())
                    .limit(RESULTS_ON_PAGE)
                    .collect(Collectors.toList());

            ResponseModel<Movie> responseModel = new ResponseModel<>();

            responseModel.setPage(invocationOnMock.getArgument(0));
            responseModel.setTotal_pages(TOTAL_PAGES);
            responseModel.setTotal_results(TOTAL_RESULTS);
            responseModel.setResults(moviesWithRandomIds);

            CompletableFuture<ResponseEntity<ResponseModel>> future = CompletableFuture.supplyAsync(() -> {
                ThreadUtils.sleep(latency);
                return new ResponseEntity<>(responseModel, HttpStatus.OK);
            });

            return TimeoutHelper.within(future, Duration.ofSeconds(TIMEOUT_SECONDS)).exceptionally(throwable -> {
                throwable.printStackTrace();
                return null;
            });
        });
    }

}