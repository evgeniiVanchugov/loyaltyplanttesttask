package integration;

import com.eugene.vanchugov.movieclient.MovieClientApplication;
import com.eugene.vanchugov.movieclient.util.ThreadUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.Input;
import org.springframework.shell.InputProvider;
import org.springframework.shell.Shell;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT_ENABLED + "=false"
}, classes = MovieClientApplication.class)
@ActiveProfiles("dev")
public class CLITest {

    @Autowired
    private Shell shell;

    @Test
    public void help() throws Exception {
        shell.run(command("help"));
    }

    @Test
    public void submit_defaultParams() throws Exception {
        shell.run(command("submit"));
        ThreadUtils.sleep(5000);
    }
    @Test
    public void submit_customParams() throws Exception {
        shell.run(command("submit -t 3 -p 12 -o 50 -r"));
        ThreadUtils.sleep(10000);
    }

    @Test
    public void cancelProcess() throws Exception {
        shell.run(command("submit"));
        //throws Exception if passed without args. Reproduces only in test, in prod works fine without args.
        shell.run(command("cancel -t 0 -u MILLISECONDS"));
    }

    @Test
    public void cancelProcess_withTimeout() throws Exception {
        shell.run(command("submit"));
        shell.run(command("cancel -t 10 -u MILLISECONDS"));
        ThreadUtils.sleep(10000);
    }

    @Test
    public void isCompleted() throws Exception {
        shell.run(command("submit"));
        shell.run(command("is-completed"));
    }

    @Test
    public void progress() throws Exception {
        shell.run(command("submit"));
        ThreadUtils.sleep(1000);
        shell.run(command("progress"));
    }

    @Test
    public void results() throws Exception {
        shell.run(command("submit"));
        ThreadUtils.sleep(1000);
        shell.run(command("results"));
    }

    private InputProvider command(String command) {
        return new InputProvider() {
            private boolean invoked = false;

            @Override
            public Input readInput() {
                if (!invoked) {
                    invoked = true;
                    return () -> command;
                } else {
                    return () -> "exit";
                }
            }
        };
    }
}
