package com.eugene.vanchugov.movieclient.command;

import com.eugene.vanchugov.movieclient.crawler.impl.CrawlerImpl;
import com.eugene.vanchugov.movieclient.crawler.impl.CrawlerProcessSingleton;
import com.eugene.vanchugov.movieclient.model.Movie;
import com.eugene.vanchugov.movieclient.model.StatusMessage;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@ShellComponent
public class CommandLineInterface {

    private final static ExecutorService EXECUTOR_THREAD = Executors.newCachedThreadPool();

    private final CrawlerProcessSingleton process;

    public CommandLineInterface() {
        process = CrawlerProcessSingleton.getInstance();
    }

    @ShellMethod("Submit job. If typed without additional args, default values will be passed. \n" +
            "\tExample:\n" +
            "\t\t\"shell:> submit\" by default will look like \"submit -t 1 -p 10 -o 1\".\n" +
            "\tThat means that 1 thread will be created; one thread will process 10 pages in a row; \n" +
            "\tcount starts from the first page.")
    public String submit(
            @ShellOption(defaultValue = "1", value = {"-t", "--threads"}, help = "Number of threads. Default value: 1 thread") int threadNum,
            @ShellOption(defaultValue = "10", value = {"-p", "--pages"}, help = "Amount of pages to process per thread. Default value: 10 pages/thread") int pagesPerThread,
            @ShellOption(defaultValue = "1", value = {"-o", "--startPage"}, help = "Initial page. Default value: 1 page") int startPage,
            @ShellOption(defaultValue = "false", value = {"-r", "--restart"}, help = "Restart failed pages. Default value: false") boolean restartFailed) {

        process.resetCancellationFlag();

        int startFromPage = startPage;

        for (int i = 0; i < threadNum; i++) {
            EXECUTOR_THREAD.submit(new SubmitJobRunnable(startPage, pagesPerThread, restartFailed, new CrawlerImpl()));
            startPage += pagesPerThread;
        }

        return "Computing in process! Threads: " + threadNum + ". Pages per thread: "+ pagesPerThread + ". Start page: " + startFromPage + ". Restart failed: " + restartFailed;
    }

    @ShellMethod("Get all results. Simply returns all what is in storage for a moment.")
    public List<Movie> results() {
        return process.getResult();
    }

    @ShellMethod("Show progress. Shows progress bar with status and amount of saved records.")
    public String progress() {
        return process.getProgressMessage();
    }

    @ShellMethodAvailability("jobStartedAvailability")
    @ShellMethod("Cancel running job. If passed without arguments, immediate canceling is called.\n" +
            "\tExample:\n" +
            "\t\t\"cancel\" - initiate immediate cancelling.\n" +
            "\n" +
            "\t\t\"cancel -t 10 -u SECONDS\" - cancelling will be started after 10 seconds.")
    public String cancel(
            @ShellOption(defaultValue = "0", value = {"-t", "--time"}, help = "Time to timeout") long timeout,
            @ShellOption(defaultValue = "", value = {"-u", "--unit"}, help = "Time units (ex. MILLISECONDS, SECONDS, HOURS, etc.)") TimeUnit timeUnit) {

        if (timeout == 0 && timeUnit == null) {
            process.cancelJob();
            return "Canceled!";
        } else process.waitForCompletion(timeout, timeUnit);
        return "Canceled with timeout of " + timeout + " " + timeUnit.toString();
    }

    @ShellMethod("Check if job is completed. Returns true if completed; false - if not")
    public String isCompleted() {
        return String.valueOf(process.isCompleted());
    }

    private Availability jobStartedAvailability() {
        return process.hasStatus(StatusMessage.IN_PROGRESS) ? Availability.available() : Availability.unavailable("job must be in progress");
    }
}
