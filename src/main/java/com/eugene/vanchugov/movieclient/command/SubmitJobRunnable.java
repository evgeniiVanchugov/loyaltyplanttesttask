package com.eugene.vanchugov.movieclient.command;

import com.eugene.vanchugov.movieclient.crawler.Crawler;
import com.eugene.vanchugov.movieclient.model.Movie;

public class SubmitJobRunnable implements Runnable {

    private final int offset;
    private final int limit;
    private final boolean restartFailed;
    private final Crawler<Movie> crawler;

    public SubmitJobRunnable(int offset, int limit, boolean restartFailed, Crawler<Movie> crawler) {
        this.offset = offset;
        this.limit = limit;
        this.restartFailed = restartFailed;
        this.crawler = crawler;
    }

    @Override
    public void run() {
        crawler.submitJob(offset, limit, restartFailed);
    }
}
