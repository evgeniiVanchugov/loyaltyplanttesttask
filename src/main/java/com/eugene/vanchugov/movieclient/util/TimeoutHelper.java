package com.eugene.vanchugov.movieclient.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.time.Duration;
import java.util.concurrent.*;
import java.util.function.Function;

public class TimeoutHelper {

    private static final int CORE_POOL_SIZE = 1;
    private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(
            CORE_POOL_SIZE,
            new ThreadFactoryBuilder()
                    .setDaemon(true)
                    .setNameFormat("failThread-%d")
                    .build());

    private static <T> CompletableFuture<T> failAfter(Duration duration) {
        final CompletableFuture<T> promise = new CompletableFuture<>();

        SCHEDULER.schedule(() -> {
            final TimeoutException ex = new TimeoutException("Timeout after " + duration.toMillis());
            return promise.completeExceptionally(ex);
        }, duration.toMillis(), TimeUnit.MILLISECONDS);

        return promise;
    }

    public static <T> CompletableFuture<T> within(CompletableFuture<T> future, Duration duration) {
        final CompletableFuture<T> timeout = failAfter(duration);
        return future.applyToEither(timeout, Function.identity());
    }
}
