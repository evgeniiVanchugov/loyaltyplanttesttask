package com.eugene.vanchugov.movieclient.crawler.impl;

import com.eugene.vanchugov.movieclient.crawler.Crawler;
import com.eugene.vanchugov.movieclient.crawler.CrawlerProcess;
import com.eugene.vanchugov.movieclient.crawler.RestTemplateRequestProvider;
import com.eugene.vanchugov.movieclient.model.Movie;
import com.eugene.vanchugov.movieclient.model.ResponseModel;
import com.eugene.vanchugov.movieclient.model.StatusMessage;
import com.eugene.vanchugov.movieclient.util.ThreadUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.eugene.vanchugov.movieclient.model.StatusMessage.COMPLETE;
import static com.eugene.vanchugov.movieclient.model.StatusMessage.IN_PROGRESS;

@Slf4j
public class CrawlerImpl implements Crawler<Movie> {

    private final Set<Integer> failedPages = new CopyOnWriteArraySet<>();
    private RestTemplateRequestProvider requestProvider;

    public CrawlerImpl() {
        this.requestProvider = new RestTemplateRequestProviderImpl();
    }

    @Override
    public CrawlerProcess<Movie> submitJob(int startPage, int limit, boolean restartFailed) {

        if (limit <= 0 || startPage <= 0) {
            throw new IllegalArgumentException("Illegal parameter");
        }

        Set<Integer> pages = IntStream.range(startPage, startPage + limit)
                .boxed()
                .filter(i -> i != 0)
                .collect(Collectors.toSet());

        CrawlerProcessSingleton process = submitJob(pages);

        if (restartFailed) {
            if (!failedPages.isEmpty()) {
                log.debug("Failed pages: {}. Trying to reload them 3 times", failedPages.size());

                for (int attempts = 0; attempts < 3; attempts++) {
                    log.debug("Waiting 3 seconds before start loading failed pages...");
                    ThreadUtils.sleep(3000);
                    process = submitJob(failedPages);

                    if (failedPages.isEmpty()) {
                        break;
                    }
                }
            }
        }

        if (!failedPages.isEmpty()) {
            log.debug("Failed pages left: {}. Try to download them later", failedPages.size());
        } else log.debug("Completed");

        if (isProcessNotInStatus(process, COMPLETE)) {
            process.setStatusMessage(COMPLETE);
        }

        return process;
    }

    private CrawlerProcessSingleton submitJob(Set<Integer> pages) {

        CrawlerProcessSingleton process = CrawlerProcessSingleton.getInstance();

        if (pages.isEmpty()) {
            log.debug("No pages to process");
            return process;
        }

        if (isProcessNotInStatus(process, IN_PROGRESS)) {
            process.setStatusMessage(IN_PROGRESS);
        }

        Iterator<Integer> pageIterator = pages.iterator();

        while (pageIterator.hasNext() && !process.isCanceled()) {

            int page = pageIterator.next();
            log.debug("Loading page {}", page);

            CopyOnWriteArraySet<Integer> savedPages = process.getSavedPages();

            if (savedPages.contains(page)) {
                log.debug("Page {} already saved. Skipping", page);
                continue;
            }

            ResponseModel<Movie> responseModel = createRequest(page);

            if (process.getTotalResults() == 0) {
                int totalResults = !Objects.isNull(responseModel.getTotal_results()) ? responseModel.getTotal_results() : 0;

                log.debug("Fetched total results amount: {}", totalResults);
                process.setTotalResults(totalResults);
            }

            List<Movie> results = responseModel.getResults();

            if (results == null) {
                log.debug("Failed to get results from server. Adding page {} into Failed Pages", page);
                failedPages.add(page);
                results = new ArrayList<>();
            } else failedPages.remove(page);

            process.getStorage().addAll(results);

            if (!failedPages.contains(page)) {
                process.savePageNumber(page);
                log.debug("Page {} saved successfully", page);
            }
        }

        return process;
    }

    private boolean isProcessNotInStatus(CrawlerProcessSingleton process, StatusMessage statusMessage) {
        return !process.getStatusMessage().equals(statusMessage.toString());
    }

    @SuppressWarnings("unchecked")
    private ResponseModel<Movie> createRequest(int page) {
        CompletableFuture<ResponseEntity<ResponseModel>> pageResponse = requestProvider.getPage(page);

        return (ResponseModel<Movie>) Optional.ofNullable(pageResponse.join())
                .map(HttpEntity::getBody)
                .orElseGet(ResponseModel::new);
    }

    public void setRequestProvider(RestTemplateRequestProvider requestProvider) {
        this.requestProvider = requestProvider;
    }
}

