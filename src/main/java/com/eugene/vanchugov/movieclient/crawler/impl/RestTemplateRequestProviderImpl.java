package com.eugene.vanchugov.movieclient.crawler.impl;

import com.eugene.vanchugov.movieclient.crawler.RestTemplateRequestProvider;
import com.eugene.vanchugov.movieclient.model.ResponseModel;
import com.eugene.vanchugov.movieclient.util.TimeoutHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class RestTemplateRequestProviderImpl implements RestTemplateRequestProvider {

    private static final String API_KEY = "72b56103e43843412a992a8d64bf96e9";
    private static final String ENDPOINT = "https://hard.test-assignment-a.loyaltyplant.net/3/discover/movie";

    @Override
    public CompletableFuture<ResponseEntity<ResponseModel>> getPage(int page) {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(ENDPOINT)
                .queryParam("api_key", API_KEY)
                .queryParam("page", page);

        String url = builder.toUriString();

        log.debug("URL: {}", url);

        CompletableFuture<ResponseEntity<ResponseModel>> future = CompletableFuture.supplyAsync(() -> restTemplate.exchange(url,
                HttpMethod.GET,
                entity,
                ResponseModel.class));

        CompletableFuture<ResponseEntity<ResponseModel>> responseWithTimeout = TimeoutHelper.within(future, Duration.ofSeconds(10));

        return responseWithTimeout.exceptionally(throwable -> {
            log.debug("Error while fetching data from server", throwable);
            return null;
        });
    }
}
