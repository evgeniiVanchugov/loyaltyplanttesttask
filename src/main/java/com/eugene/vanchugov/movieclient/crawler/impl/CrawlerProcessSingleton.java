package com.eugene.vanchugov.movieclient.crawler.impl;

import com.eugene.vanchugov.movieclient.crawler.CrawlerProcess;
import com.eugene.vanchugov.movieclient.model.Movie;
import com.eugene.vanchugov.movieclient.model.StatusMessage;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class CrawlerProcessSingleton implements CrawlerProcess<Movie> {

    private static final CopyOnWriteArraySet<Movie> STORAGE = new CopyOnWriteArraySet<>();

    private static CrawlerProcessSingleton PROCESS_SINGLETON_INSTANCE;
    private static StatusMessage statusMessage = StatusMessage.NOT_STARTED;

    private final AtomicBoolean isCanceled = new AtomicBoolean(false);
    private final AtomicInteger totalResults = new AtomicInteger(0);
    private final CopyOnWriteArraySet<Integer> savedPages = new CopyOnWriteArraySet<>();

    private CrawlerProcessSingleton() {
        if (PROCESS_SINGLETON_INSTANCE != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance");
        }
    }

    public static CrawlerProcessSingleton getInstance() {

        if (PROCESS_SINGLETON_INSTANCE == null) {

            synchronized (CrawlerProcessSingleton.class) {
                if (PROCESS_SINGLETON_INSTANCE == null) {
                    PROCESS_SINGLETON_INSTANCE = new CrawlerProcessSingleton();
                }
            }
        }
        return PROCESS_SINGLETON_INSTANCE;
    }

    @Override
    public float getProgress() {
        int amount = totalResults.get();

        if (amount == 0) {
            return 0;
        }

        return (float) getResult().size() / amount;
    }

    @Override
    public String getStatusMessage() {
        return statusMessage.toString();
    }

    @Override
    public boolean waitForCompletion(long timeout, TimeUnit timeUnit) {
        log.debug("Cancel with timeout {} {} initiated.", timeout, timeUnit.name());

        ScheduledExecutorService delayedCancel = Executors.newScheduledThreadPool(1);

        delayedCancel.schedule(this::cancelJob, timeout, timeUnit);

        return true;
    }

    @Override
    public boolean isCompleted() {
        return statusMessage.equals(StatusMessage.COMPLETE);
    }

    @Override
    public void cancelJob() {
        isCanceled.compareAndSet(false, true);
        setStatusMessage(StatusMessage.CANCELED);
    }

    @Override
    public List<Movie> getResult() {
        return new ArrayList<>(STORAGE);
    }

    public boolean hasStatus(StatusMessage message) {
        return statusMessage.equals(message);
    }

    public void resetCancellationFlag() {
        log.debug("Reset cancellation flag back into false");
        isCanceled.compareAndSet(true, false);
    }

    public void setTotalResults(int amount) {
        totalResults.compareAndSet(0, amount);
    }

    void setStatusMessage(StatusMessage message) {
        statusMessage = message;
    }

    void savePageNumber(int page) {
        savedPages.add(page);
    }

    int getTotalResults() {
        return totalResults.get();
    }

    boolean isCanceled() {
        return isCanceled.get();
    }

    public CopyOnWriteArraySet<Movie> getStorage() {
        return STORAGE;
    }

    CopyOnWriteArraySet<Integer> getSavedPages() {
        return savedPages;
    }
}
