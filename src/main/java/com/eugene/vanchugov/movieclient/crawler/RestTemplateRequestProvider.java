package com.eugene.vanchugov.movieclient.crawler;

import com.eugene.vanchugov.movieclient.model.ResponseModel;
import org.springframework.http.ResponseEntity;

import java.util.concurrent.CompletableFuture;

public interface RestTemplateRequestProvider {

    CompletableFuture<ResponseEntity<ResponseModel>> getPage(int page);
}
