package com.eugene.vanchugov.movieclient.crawler;

public interface Crawler<T> {

    /**
     *
     * @param startPage - стартовая страница (начинается с 1)
     * @param limit - количество страниц
     * @param restartFailed -  хэндлить ли фэйловые страницы
     * @return
     */
    CrawlerProcess<T> submitJob(int startPage, int limit, boolean restartFailed);
}
