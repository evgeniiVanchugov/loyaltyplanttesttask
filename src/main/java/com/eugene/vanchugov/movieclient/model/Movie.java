package com.eugene.vanchugov.movieclient.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode
public class Movie {

    private int id;
    private int voteCount;
    private boolean video;
    private float voteAverage;
    private String title;
    private float popularity;
    private String originalLanguage;
    private String originalTitle;
    private int[] genreIds;
    private boolean adult;
    private String overview;
    private LocalDate releaseDate;
}
