package com.eugene.vanchugov.movieclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseModel<T> {

    private Integer page;
    private Integer total_results;
    private Integer total_pages;
    private List<T> results;
}
