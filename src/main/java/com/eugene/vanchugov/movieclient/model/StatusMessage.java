package com.eugene.vanchugov.movieclient.model;

public enum  StatusMessage {
    NOT_STARTED,
    COMPLETE,
    IN_PROGRESS,
    CANCELED;
}
